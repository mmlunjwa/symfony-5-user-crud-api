<?php


namespace App\Service;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Serializer\Serializer;

class DaoFactory
{
    private static $_instance;

    public function __construct()
    {
    }

    /**
     * Set the factory instance
     * @param DaoFactory $f
     */
    public static function setFactory(DaoFactory $f)
    {
        self::$_instance = $f;
    }

    /**
     * Get a factory instance.
     * @return DaoFactory
     */
    public static function getFactory(): DaoFactory
    {
        if(!self::$_instance)
            self::$_instance = new self;

        return self::$_instance;
    }


    /**
     * @param ManagerRegistry $entityManagerRegistry
     * @return UserDaoMysql
     */
    public function getUserDao(ManagerRegistry $entityManagerRegistry): UserDaoMysql
    {
        return new UserDaoMysql($entityManagerRegistry);
    }

    /**
     * @return UserDaoJson
     */
    public function getUserDaoJson(): UserDaoJson
    {
        return new UserDaoJson();
    }

    /**
     * @param DocumentManager $documentManager
     * @return UserDaoMongoDb
     */
    public function getUserDaoMongoDb(DocumentManager $documentManager): UserDaoMongoDb
    {
        return new UserDaoMongoDb($documentManager);
    }

    /**
     * @return UserDaoXml
     */
    public function getUserDaoXml(): UserDaoXml
    {
        return new UserDaoXml();
    }

}