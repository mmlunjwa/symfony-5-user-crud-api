<?php


namespace App\Service;

use Doctrine\ODM\MongoDB\MongoDBException;

use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;

class UserDaoMongoDb implements UserDaoInterface
{

    /**
     * @var DocumentManager
     */
    private $documentManager;


    /**
     * UserDaoJson constructor.
     */
    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager  = $documentManager;
    }

    /**
     * @param UserDataModel $userDataModel
     */
    public function add(UserDataModel $userDataModel)
    {
        $user  =  new User();
        $user->setFirstName($userDataModel->getFirstName());
        $user->setLastName($userDataModel->getLastName());
        $user->setEmail($userDataModel->getEmail());
        $user->setPhoneNumber($userDataModel->getPhoneNumber());

        $this->documentManager->persist($user);
    }

    /**
     * @param $id
     * @return object|null
     */
    public function get($id): ?object
    {
       return $this->documentManager->getRepository(User::class)->findOneBy(['id'=>$id]);
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->documentManager->getRepository(User::class)->findAll();
    }

    /**
     * @throws MongoDBException
     */
    public function update($user)
    {
         $this->documentManager->persist($user);
         $this->documentManager->flush();
    }

    /**
     * @throws MongoDBException
     */
    public function delete($user)
    {
        $this->documentManager->remove($user);
        $this->documentManager->flush();
    }
}