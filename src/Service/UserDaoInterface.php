<?php


namespace App\Service;


use App\Entity\User;

interface UserDaoInterface
{
    public function get($id);
    public function getAll();
}