<?php


namespace App\Service;


use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectRepository;

class UserDaoMysql implements UserDaoInterface
{

    /**
     * @var ManagerRegistry
     */
    protected $emr;

    /**
     * @var ObjectRepository
     */
    private $userRepository;


    /**
     * @param ManagerRegistry $entityManagerRegistry
     */
    public function __construct(ManagerRegistry $entityManagerRegistry)
    {
        $this->emr = $entityManagerRegistry;
        $this->userRepository = $this->emr->getRepository(User::class);
    }

    /**
     * Insert a new user into the database
     * @param UserDataModel $userDataModel
     */
    public function add(UserDataModel $userDataModel)
    {
        $user = new \App\Entity\User();
        $user->setFirstName($userDataModel->getFirstName());
        $user->setLastName($userDataModel->getLastName());
        $user->setPhoneNumber($userDataModel->getPhoneNumber());
        $user->setEmail($userDataModel->getEmail());

        $this->userRepository->saveUser($user);
    }

    /**
     * @param $id
     * @return Object|null
     */
    public function get($id): ?Object
    {
        return  $this->userRepository->findOneBy(['id' => $id]);
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return  $this->userRepository->findAll();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return  $this->userRepository->updateUser($user);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return  $this->userRepository->removeUser($user);
    }
}