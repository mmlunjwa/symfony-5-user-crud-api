<?php


namespace App\Service;


class UserDaoJson implements UserDaoInterface
{

    /**
     * @var false|string
     */
    private $data;

    /**
     * UserDaoJson constructor.
     */
    public function __construct()
    {
        $this->data = file_get_contents(__DIR__.'/../../assets/json/users.json');
    }

    /**
     * @param $id
     * @return UserDataModel
     */
    public function get($id): ?UserDataModel
    {
        $users = json_decode($this->data,1);

        foreach ($users as $user) {
            if($id == $user['id'] ){
                $userData = new UserDataModel();
                $userData->setFirstName($user['firstName']);
                $userData->setLastName($user['lastName']);
                $userData->setPhoneNumber($user['email']);
                $userData->setPhoneNumber($user['phoneNumber']);
                $userData->setId($user['id']);
                return $userData;
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $users = json_decode($this->data,1);

        $data = [];
        foreach ($users as $user) {
            $userData = new UserDataModel();
            $userData->setFirstName($user['firstName']);
            $userData->setLastName($user['lastName']);
            $userData->setPhoneNumber($user['email']);
            $userData->setPhoneNumber($user['phoneNumber']);
            $userData->setId($user['id']);
           $data[] = $userData;
        }
        return $data;
    }

}