<?php


namespace App\Service;

use Symfony\Component\Serializer\Serializer;

class UserDaoXml implements UserDaoInterface
{

    /**
     * @var false|string
     */
    private $data;


    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * UserDaoJson constructor.
     */
    public function __construct()
    {
        $this->data = file_get_contents(__DIR__.'/../../assets/xml/users.xml');
    }

    /**
     * @param $id
     * @return UserDataModel|false|\SimpleXMLElement|string|null
     */
    public function get($id)
    {
        $data = simplexml_load_string($this->data);
        $json = json_encode($data);
        $elements = json_decode($json,TRUE);
        $elements = $elements['element'];

        foreach ($elements as $element) {
            if($id == $element['id'] ) {
                $userData = new UserDataModel();
                $userData->setFirstName($element['firstName']);
                $userData->setLastName($element['lastName']);
                $userData->setEmail($element['email']);
                $userData->setPhoneNumber($element['phoneNumber']);
                $userData->setId($element['id']);
                return $userData;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $data = simplexml_load_string($this->data);
        $json = json_encode($data);
        $elements = json_decode($json,TRUE);
        $elements = $elements['element'];
        $data = [];
        foreach ($elements as $element) {
            $userData = new UserDataModel();
            $userData->setFirstName($element['firstName']);
            $userData->setLastName($element['lastName']);
            $userData->setEmail($element['email']);
            $userData->setPhoneNumber($element['phoneNumber']);
            $userData->setId($element['id']);
            $data[] = $userData;
        }

        return $data;
    }

}