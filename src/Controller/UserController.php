<?php


namespace App\Controller;

use App\Service\DaoFactory;
use App\Service\UserDataModel;
use App\Service\UserDaoInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController
{

    /**
     * @var UserDaoInterface
     */
    private $userDao;


    /**
     * UserController constructor.
     * @param ManagerRegistry $entityManagerRegistry
     * @param DocumentManager $documentManager
     * @param string $dataSource
     */
    public function __construct(ManagerRegistry $entityManagerRegistry, DocumentManager $documentManager, string $dataSource = 'mysql')
    {
        if($dataSource == 'mysql'){
            $this->userDao = DaoFactory::getFactory()->getUserDao($entityManagerRegistry);
        }

        if($dataSource == 'mongo'){
            $this->userDao = DaoFactory::getFactory()->getUserDaoMongoDb($documentManager);
        }

        if($dataSource == 'json'){
            $this->userDao = DaoFactory::getFactory()->getUserDaoJson();
        }

        if($dataSource == 'xml'){
            $this->userDao = DaoFactory::getFactory()->getUserDaoXml();
        }
    }

    /**
     * @Route("/users/", name="add_user", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $email = $data['email'];
        $phoneNumber = $data['phoneNumber'];

        if (empty($firstName) || empty($lastName) || empty($email) || empty($phoneNumber)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $userDataModel = new UserDataModel();
        $userDataModel->setFirstName($firstName);
        $userDataModel->setLastName($lastName);
        $userDataModel->setEmail($email);
        $userDataModel->setPhoneNumber($phoneNumber);

        $this->userDao->add($userDataModel);

        return new JsonResponse(['status' => 'User created!'], Response::HTTP_CREATED);
    }


    /**
     * @Route("/users/{id}", name="get_one_user", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $user = $this->userDao->get($id);

        if(isset($user)){
            $data = [
                'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'email' => $user->getEmail(),
                'phoneNumber' => $user->getPhoneNumber(),
            ];

            return new JsonResponse($data, Response::HTTP_OK);
        }else{
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * @Route("/users", name="get_all_users", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $users = $this->userDao->getAll();
        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'email' => $user->getEmail(),
                'phoneNumber' => $user->getPhoneNumber(),
            ];
        }
//        dd($users);
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/users/{id}", name="update_user", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $user = $this->userDao->get($id);
        $data = json_decode($request->getContent(), true);

        empty($data['firstName']) ? true : $user->setFirstName($data['firstName']);
        empty($data['lastName']) ? true : $user->setLastName($data['lastName']);
        empty($data['email']) ? true : $user->setEmail($data['email']);
        empty($data['phoneNumber']) ? true : $user->setPhoneNumber($data['phoneNumber']);

        $updatedUser = $this->userDao->update($user);

        return new JsonResponse($updatedUser->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/users/{id}", name="delete_user", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $user = $this->userDao->get($id);
        $this->userDao->delete($user);

        return new JsonResponse(['status' => 'User deleted'], Response::HTTP_NO_CONTENT);
    }

}