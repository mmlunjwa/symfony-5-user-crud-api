#UserApi 
UserApi is a great way of demonstrating a scalable REST API that uses the Data access object design pattern.

#Description
Write a simple API for showing a list of users, and creating a new user. The goal is to
be able to swap out the data source for users without having to touch any of the code
that uses the data source and returns the response.

#Requirements:
● PHP 7.2

#Setup

Modify the DATABASE_URL in .env file to match your database user settings

Create a database named: rest_api

Start the App run:

    symfony server:start

run migrations:

    bin/console doctrine:migrations:migrate

To load data fixtures run:

    bin/console doctrine:fixtures:load


##Using Data Access Object pattern to decouple models from the database

The Data Access Object pattern abstracts the details of the storage mechanism – be it a relational database, Document database, an XML file or whatever. 
The advantage of this is that you can easily implement different methods to persist objects without having to rewrite parts of your code.

App/services has the following:

Interface  -> UserDaoInterface

Data Access Object -> UserDaoMysql, UserDaoMongoDb, UserDaoXml, UserDaoJson

Data Access Object Factory -> DaoFactory

Data Model/Value -> UserDataModel

The UserDaoInterface provides methods for accessing the data.

    public function get($id);
    public function getAll();

Mysql Database
UserDaoMysql implements UserDaoInterface and adds the add, update and delete methods. UserDaoMysql interacts with the Mysql database

To add another way of accessing data, you simply write a new set of Data Access Object’s that implement the UserDaoInterface. I've added 3 
more different data sources to demonstrate this

Mongo Database
UserDaoMongoDb implements UserDaoInterface and adds the add, update and delete methods. UserDaoMongoDb interacts with the Mongo Database

Xml File
UserDaoXml implements UserDaoInterface. UserDaoXml interacts with App/assets/xml/users.xml file 

Json File
UserDaoXml implements UserDaoInterface. UserDaoXml interacts with App/assets/xml/users.xml file

The DaoFactory handles the creation of the UserDaoDoctrine.

UserDataModel used to set and get user values without persistence-related logic.

The UserController construct has a dataSource parameter, You can toggle between 4 different data sources (Mysl, MongoDb, xml and Json)
Mysql and MongoDb has the full crud (add, getById, getAll, update and delete)functionality. xml and Json has read functionality (getById, getAll)

#USER API
#Add User:
Method: POST
Url:  https://127.0.0.1:8000/users/

Json body:
{
"firstName":"Mthabisi",
"lastName":"Mlunjwa",
"email":"mmlunjwa@gmail.com",
"phoneNumber":"(317) 856-4476"
}


#Get user
Method: GET
Url:  https://127.0.0.1:8000/users/{id}


#List all users
Method: GET
Url:  https://127.0.0.1:8000/users


#Update User:
Method: PUT
Url:  https://127.0.0.1:8000/users/{id}

Json body:
{
"lastName":"Mlunjwana",
"email":"mmlunjwa+1@gmail.com",
"phoneNumber":"086 244 7689"
}

#Delete User:
Method: DELETE
Url:  https://127.0.0.1:8000/users/{id}

